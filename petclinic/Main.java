package petclinic;

import petclinic.dao.ConsultDao;
import petclinic.dao.PetDao;
import petclinic.dao.VeterinarianDao;
import petclinic.model.Consult;
import petclinic.model.Veterinarian;

import java.util.List;

public class Main {
    public static void main(String[] args) {

//        Session session = HibernateUtils.getSessionFactory().openSession();
//        session.close();

        VeterinarianDao vetDao = new VeterinarianDao();
//        List<Veterinarian> veterinarianList = vetDao.getAllVeterinarians();
//        System.out.println(veterinarianList);
//
        Veterinarian veterinarian = new Veterinarian("Stephan", "Smith", "8th London Street", "cardiology");
//        vetDao.add(veterinarian);
//        System.out.println(veterinarian);

//        Veterinarian aVet = vetDao.findById(8L);
//        System.out.println("Veterinarul cautat este " + aVet);
//
//        aVet.setLastName("Black");
//        vetDao.update(aVet);
//        System.out.println("Veterinarul dupa actualizarea datelor este" + aVet);

//        Veterinarian veterinarian2 = new Veterinarian("Josh", "First", "2nd Paris Street", "surgery");
//        vetDao.add(veterinarian2);
//        veterinarianList= vetDao.getAllVeterinarians();
//        System.out.println(veterinarianList);

//        vetDao.delete(aVet);
//        System.out.println(aVet);

        PetDao petDao = new PetDao();
//        List<Pet> petList = petDao.getAllPets();
//        System.out.println("Lista de pets este " + petList);

        /* in cazul in care nu mergea cu Date.valueOf()

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date dateRepresentation = cal.getTime();

        Date d1 = new Date();
        Pet aNewPet = new Pet("german dog", d1, true, "Derek Keys");

         */

//        Pet aNewPet = new Pet();
//        aNewPet.setRace("german dog");
//        aNewPet.setBirthdate(Date.valueOf("2020-02-03"));
//        aNewPet.setIsvaccinated(true);
//        aNewPet.setOwnerName("Derek Keys");
//        petDao.create(aNewPet);
//        System.out.println("Noul animal introdus este " + aNewPet);

//        Pet aPet = petDao.findById(1L);
//        System.out.println("Animalul cautat este " + aPet);

//        petDao.delete(aPet);
//        System.out.println("Animalul cautat este " + aPet);

//        aPet.setOwnerName("Lilian WinterStone");
//        petDao.update(aPet);
//        System.out.println("Animalul cautat dupa insert este " + aPet);

        ConsultDao consultDao = new ConsultDao();
        List<Consult> consultList = consultDao.getAllConsults();
        System.out.println("Lista de consultatii este " + consultList);

//        Veterinarian consultatieNouaPentruVeterinar = vetDao.findById(1L);
//        Pet consultatieNouaPentruPet = petDao.findById(1L);
//        Consult consult = new Consult(Date.valueOf("2021-12-11"), "pacientul vine pentru consult periodic de rutina", consultatieNouaPentruVeterinar, consultatieNouaPentruPet);
//        consultDao.addConsult(consult);
//        System.out.println("Consultatia noua este " + consult);

//        Consult consultUpdateDesCription = consultDao.findById(3L);
//        consultUpdateDesCription.setDescription("pacientul revine la control");
//        consultDao.updateConsult(consultUpdateDesCription);
//        System.out.println("Descrierea noua " + consultUpdateDesCription);



    }
}
