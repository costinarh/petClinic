package petclinic.menu;

import org.hibernate.Session;
import petclinic.HibernateUtils;

public class Main {
    public static void main(String[] args) {

    Session session = HibernateUtils.getSessionFactory().openSession();
    session.close();

    Menu menu = new Menu();
    int option = 1;
        while (option != 0) {
            menu.displayMenu();
            option = menu.chooseOption();
        }
    }
}
