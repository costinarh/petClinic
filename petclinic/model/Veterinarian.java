package petclinic.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "veterinarians")
public class Veterinarian {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column
    private String speciality;

    @Column
    private String address;

    @OneToMany(mappedBy = "veterinarian")
    private List<Consult> veterinarianConsultList;

    public Veterinarian() {
    }

    public Veterinarian(String firstName, String lastName, String speciality, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.speciality = speciality;
        this.address = address;

    }

    public Veterinarian(String firstName, String lastName, String address, String speciality, List<Consult> veterinarianConsultList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.speciality = speciality;
        this.veterinarianConsultList = veterinarianConsultList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public List<Consult> getVeterinarianConsultList() {
        return veterinarianConsultList;
    }

    public void setVeterinarianConsultList(List<Consult> veterinarianConsultList) {
        this.veterinarianConsultList = veterinarianConsultList;
    }

    @Override
    public String toString() {
        return "Veterinarian{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", speciality='" + speciality + '\'' +
                "}\n";
    }
}
