package petclinic.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "consults")
public class Consult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long consultId;

    @Column
    private Date date;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "veterinarianId")
    private Veterinarian veterinarian;

    @ManyToOne
    @JoinColumn(name = "petId")
    private Pet pet;

    public Consult() {
    }

    public Consult(Date date, String description, Veterinarian veterinarian, Pet pet) {
        this.date = date;
        this.description = description;
        this.veterinarian = veterinarian;
        this.pet = pet;
    }

    public long getConsultId() {
        return consultId;
    }

    public void setConsultId(long consultId) {
        this.consultId = consultId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Veterinarian getVeterinarian() {
        return veterinarian;
    }

    public void setVeterinarian(Veterinarian veterinarian) {
        this.veterinarian = veterinarian;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Consult{" +
                "consultId=" + consultId +
                ", date=" + date +
                ", description='" + description + '\'' +
                "}\n";
    }
}
