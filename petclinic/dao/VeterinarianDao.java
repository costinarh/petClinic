package petclinic.dao;

import petclinic.model.Veterinarian;
import org.hibernate.Session;
import petclinic.HibernateUtils;

import java.util.List;

public class VeterinarianDao extends VetAndPetDao<Veterinarian> {

    public List<Veterinarian> getAllVeterinarians() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Veterinarian> veterinarians = session.createQuery("from Veterinarian", Veterinarian.class).list();
            return veterinarians;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public Veterinarian findById (long id){
        try{
            Session session = HibernateUtils.getSessionFactory().openSession();
            Veterinarian vet = session.find(Veterinarian.class, id);
            session.close();
            return vet;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}

