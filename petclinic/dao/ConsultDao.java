package petclinic.dao;
import petclinic.model.Consult;
import org.hibernate.Session;
import org.hibernate.Transaction;
import petclinic.HibernateUtils;

import java.util.List;

public class ConsultDao {

    public List<Consult> getAllConsults() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Consult> consults = session.createQuery("from Consult", Consult.class).list();
            return consults;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void addConsult (Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(consult);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void updateConsult (Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(consult);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Consult findById (long id){
        try{
            Session session = HibernateUtils.getSessionFactory().openSession();
            Consult consult = session.find(Consult.class, id);
            session.close();
            return consult;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

}
