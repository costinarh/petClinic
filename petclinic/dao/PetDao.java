package petclinic.dao;

import org.hibernate.Session;
import petclinic.HibernateUtils;
import petclinic.model.Pet;

import java.util.List;

public class PetDao extends VetAndPetDao<Pet>{

    public List<Pet> getAllPets() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Pet> pets = session.createQuery("from Pet", Pet.class).list();
            return pets;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Pet findById (long id){
        try{
            Session session = HibernateUtils.getSessionFactory().openSession();
            Pet pet = session.find(Pet.class, id);
            session.close();
            return pet;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

}